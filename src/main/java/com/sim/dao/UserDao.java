package com.sim.dao;

import com.sim.model.User;

public interface UserDao {
	
	public User getUser(String login);

}
