package com.sim.dao;

import com.sim.model.Role;

public interface RoleDao {
	
	public Role getRole(int id);

}
