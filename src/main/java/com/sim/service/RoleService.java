package com.sim.service;

import com.sim.model.Role;

public interface RoleService {
	
	public Role getRole(int id);

}
