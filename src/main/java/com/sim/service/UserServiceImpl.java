package com.sim.service;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sim.dao.UserDao;
import com.sim.model.User;

@Service
@Transactional
public class UserServiceImpl implements UserService {
	
	@Autowired
	private UserDao userDAO;

	public User getUser(String login) {
		return userDAO.getUser(login);
	}

}
