package com.sim.service;

import com.sim.model.User;

public interface UserService {
	
	public User getUser(String login);

}
